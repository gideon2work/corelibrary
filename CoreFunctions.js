/**
 * Created by gd on 4/20/17.
 */
function doGet(e){
    //Logger.log(eval('getAllSpreadsheets')());
    var func = e.parameter.func ? e.parameter.func : 'getAllSpreadsheets';
    var content = eval(func)();
    if(e.parameter.email) sendEmail(e.parameter.email,'Gideon:利用Google WebApp，读取Drive中所有Spreadsheet，按其修改时间降序排列',content);
    return HtmlService.createHtmlOutput(content);
}

function getAllSpreadsheets() {
    var output = '<!DOCTYPE html><html><head><base target="_top"></head><style>table{border-collapse: collapse;}table, th, td {border: 1px solid black;}</style><body>';
    output += '<h2 style="text-align:center;">WebApp 网页链接请点击这里：';
    output += '<a href="https://script.google.com/macros/s/AKfycbxnXgI9SPOu5D1c4yU8U7UB8n2RAwSrKcXRgmpHgnZSAtK8OWQ/exec?func=getAllSpreadsheets">打开网页版</a>';
    output += '<br>参数func是需要调用的内部函数名，email是接收数据的邮箱，没有设置的话，默认不发送邮件</h2>';
    output += '<table><tr><th colspan="4">List All Spreadsheets Frome Gideon2work@gmail.com\'s Google Drive </th></tr>';
    output += '<tr><th>No.</th><th>File Name</th><th>File Url</th><th>File Created Date</th></tr>';

    var files = DriveApp.getFilesByType(MimeType.GOOGLE_SHEETS);

    var result = [];
    while (files.hasNext()) {
        var file = files.next();
        result.push([Utilities.formatDate(file.getDateCreated(), 'America/Los_Angeles', 'yyyy-MM-dd'),file.getName(),file.getUrl()]);
    }
    result = result.sort().reverse();

    //Logger.log(result);

    var total = result.length;
    for (var i=0; i<total; i++) {
        output += '<tr><td>'+(i*1+1)+'</td><td>'+result[i][1]+'</td><td>'+result[i][2]+'</td><td>'+result[i][0]+'</td></tr>';
    }
    return output += '</table></body></html>';
}

function sendEmail(receipt,subject,data){
    //var receipt = 'gideon2work@gmail.com', subject = 'this is a test!', data = '<h1>this is a test from my task</h1>';
    MailApp.sendEmail({to: receipt, subject: subject, htmlBody: data});
}

function onOpen() {
    var sheet = SpreadsheetApp.getActiveSpreadsheet();
    var entries = [
        {
            name : "Copy Template Sheet",
            functionName : "copyTemplate"
        },
    ];
    sheet.addMenu("Custom Functions", entries);
}

function copyTemplate()
{
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    ss.insertSheet(Utilities.formatDate(new Date(), 'America/Los_Angeles', 'MM/dd'),1,{template: ss.getSheetByName("Template")});
}

